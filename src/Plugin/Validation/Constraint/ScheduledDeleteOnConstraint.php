<?php
/**
 * Scheduled Delete - Scheduled delete content nodes
 *
 * @package     scheduled_delete
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        https://fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        7/31/2019
 * Time:        12:06 PM
 */

namespace Drupal\scheduled_delete\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Validates node scheduled delete date values.
 *
 * @Constraint(
 *   id = "ScheduledDeleteOn",
 *   label = @Translation("Scheduled Delete On", context = "Validation"),
 *   type = "entity:node"
 * )
 */
class ScheduledDeleteOnConstraint extends CompositeConstraintBase {

  /**
   * Message shown when node_expire_on is not the future.
   *
   * @var string
   */
  public $messageScheduledDeleteOnDateNotInFuture = "The 'scheduled delete' date must be in the future.";

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['scheduled_delete'];
  }

}
