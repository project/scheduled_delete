<?php
/**
 * Scheduled Delete - Scheduled delete content nodes
 *
 * @package     scheduled_delete
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        https://fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        7/31/2019
 * Time:        12:06 PM
 */

namespace Drupal\scheduled_delete\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ScheduledDeleteOn constraint.
 */
class ScheduledDeleteOnConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    $scheduled_delete = $entity->value;
    if ($scheduled_delete && $scheduled_delete < REQUEST_TIME) {
      $this->context->buildViolation($constraint->messageScheduledDeleteOnDateNotInFuture)
        ->atPath('scheduled_delete')
        ->addViolation();
    }
  }

}
