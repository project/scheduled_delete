<?php
/**
 * Scheduled Delete - Scheduled delete content nodes
 *
 * @package     scheduled_delete
 * @author      Udit Rawat <uditrawat@fabwebstudio.com>
 * @license     GPL-2.0+
 * @link        https://fabwebstudio.com/
 * @copyright   FabWebStudio
 * Date:        7/31/2019
 * Time:        12:06 PM
 */

namespace Drupal\scheduled_delete;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\NodeType;

/**
 * Defines a Scheduled delete manager.
 */
class ScheduledDeleteManager {

  /**
   * Entity type manager service object
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ScheduledDeleteManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
  }

  public function delete() {
    // Select all nodes of the types that are enabled for scheduled deleting
    // and where scheduled_delete is less than or equal to the current time.
    $nids = [];
    $sd_enabled_types = array_keys($this->get_enabled_node_types());
    if (!empty($sd_enabled_types)) {
      // Todo: Inject Entity Query Instead.
      $query = \Drupal::entityQuery('node')
        ->exists('scheduled_delete')
        ->condition('scheduled_delete', \Drupal::time()->getRequestTime(), '<=')
        ->condition('type', $sd_enabled_types, 'IN')
        ->sort('scheduled_delete')
        ->sort('nid');
      $query->accessCheck(FALSE);
      $nids = $query->execute();
    }

    // run auto delete in 1 week
    $auto_enabled_types = array_keys($this->get_enabled_node_types(TRUE));
    if(!empty($auto_enabled_types)){
      $auto_query = \Drupal::entityQuery('node')
        ->condition('changed', strtotime('-1 week'), '<=')
        ->condition('type', $auto_enabled_types, 'IN')
        ->sort('nid');
      $auto_query->accessCheck(FALSE);
      $aq_nids = $auto_query->execute();

      // Merge array
      $nids = array_merge($nids, $aq_nids);
    }

    // Delete all
    if (is_array($nids)) {
      $this->deleteNodeGroup($nids);
    }
  }

  /**
   * Returns all content types for which scheduled delete has been enabled.
   * Returns all content types for which scheduled delete has been enabled
   * for schedule delete in 1 week.
   *
   * @param bool $auto_delete
   *
   * @return \Drupal\node\NodeTypeInterface[]
   *   Array of NodeTypeInterface objects
   */
  public function get_enabled_node_types($auto_delete = FALSE) {
    $config = $this->configFactory->get('scheduled_delete.settings');
    $node_types = NodeType::loadMultiple();
    // If auto delete is on
    if ($auto_delete) {
      return array_filter($node_types, function ($bundle) use ($config) {
        /* @var \Drupal\node\NodeTypeInterface $bundle */
        return (
          $bundle->getThirdPartySetting('scheduled_delete', 'sd_enable', $config->get('sd_enable')) &&
          $bundle->getThirdPartySetting('scheduled_delete', 'sd_auto_delete', $config->get('sd_auto_delete'))
        ) ? TRUE : FALSE;
      });
    }
    else {
      // auto delete is false
      return array_filter($node_types, function ($bundle) use ($config) {
        /* @var \Drupal\node\NodeTypeInterface $bundle */
        return (
          $bundle->getThirdPartySetting('scheduled_delete', 'sd_enable', $config->get('sd_enable')) &&
          !$bundle->getThirdPartySetting('scheduled_delete', 'sd_auto_delete', $config->get('sd_auto_delete'))
        ) ? TRUE : FALSE;
      });
    }
  }

  /**
   * Delete node group
   *
   * @param array $node_ids
   */
  protected function deleteNodeGroup(array $node_ids) {
    try {
      $storage_handler = $this->entityTypeManager->getStorage("node");
      // load nodes
      $entities = $storage_handler->loadMultiple($node_ids);
      // delete nodes
      $storage_handler->delete($entities);

    } catch (\Exception $e) {
    }
  }

}